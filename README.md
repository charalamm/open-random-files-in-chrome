# Open Files In Chrome

A windows desktop app that opens a selected number of files that exist in a selected folder in Chrome.

![App-Screenshot](./docs/assets/App-Screenshot.png)

## Installation
1. Download the latest release from the [releases page](https://gitlab.com/charalamm/open-random-files-in-chrome/-/releases)
2. Extract the downloaded zip file.
3. Run the `OpenFilesInChrome.exe` file.

## Supported File Types
The supported file types are:
* `.jpeg`
* `.jpg`
* `.png`
* `.webm`
* `.webp`
* `.gif`
* `.mp4`
