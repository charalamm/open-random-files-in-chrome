import json
import os
import wx

import open_files
from random_files import type_selector

class MainFrame(wx.Frame):

    def __init__(self):

        wx.Frame.__init__(self, None, title="Open files in Chrome")

        defaults = self.get_defaults()

        sizer = wx.BoxSizer(wx.VERTICAL)

        # Create a panel
        p = wx.Panel(self)

        # Empty Space
        sizer.Add(wx.StaticLine(p, size=wx.Size(0,0)), 0, wx.ALL | wx.EXPAND, 10)

        # Dir Picker
        file_sizer = wx.BoxSizer(wx.HORIZONTAL)
        excel_label = wx.StaticText(p, 0, "Folder Path ")
        file_sizer.Add(excel_label, 0, wx.ALL | wx.EXPAND, 5)
        self.dir_picker = wx.DirPickerCtrl(p, path=defaults["path"])
        file_sizer.Add(self.dir_picker, 0, wx.ALL , 5)
        sizer.Add(file_sizer, 0, wx.ALL , 5)

        # Subdir checkbox
        checkbox_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.search_subdirs_checkbox = wx.CheckBox(p, label='Search subfolders')
        self.search_subdirs_checkbox.SetValue(defaults['subdir'])
        checkbox_sizer.Add(self.search_subdirs_checkbox, 0, wx.ALL | wx.EXPAND, 5)
        self.include_mp4_checkbox = wx.CheckBox(p, label='include mp4')
        checkbox_sizer.Add(self.include_mp4_checkbox, 0, wx.ALL, 5)
        self.only_mp4_checkbox = wx.CheckBox(p, label='only mp4')
        checkbox_sizer.Add(self.only_mp4_checkbox, 0, wx.ALL, 5)
        sizer.Add(checkbox_sizer, 0, wx.ALL , 5)

        # Empty Space
        wordsearch_sizer = wx.BoxSizer(wx.HORIZONTAL)
        wordsearch_label = wx.StaticText(p, 0, "Search keyword ")
        wordsearch_sizer.Add(wordsearch_label, 0, wx.ALL | wx.EXPAND, 5)
        self.wordsearch_textbox = wx.TextCtrl(p,)
        wordsearch_sizer.Add(self.wordsearch_textbox, 0, wx.ALL , 5)
        sizer.Add(wordsearch_sizer, 0, wx.ALL, 5)
        #sizer.Add(wx.StaticLine(p, size=wx.Size(0,0)), 0, wx.ALL | wx.EXPAND, 10)

        # Number of files interval
        num_files_sizer = wx.BoxSizer(wx.HORIZONTAL)
        num_files_label = wx.StaticText(p, 0, "Number of files to open ")
        num_files_sizer.Add(num_files_label, 0, wx.ALL , 5)
        self.num_files_entry = wx.SpinCtrl(p, value=str(defaults['number_of_files']), min=1)
        num_files_sizer.Add(self.num_files_entry, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add(num_files_sizer, 0, wx.ALL | wx.EXPAND, 5)

        # Open files in browser
        open_files_btn = wx.Button(p, label='Open files')
        open_files_btn.Bind(wx.EVT_BUTTON, self.open_files)
        sizer.Add(open_files_btn, 0, wx.ALL | wx.EXPAND, 5)

        # Empty Space
        sizer.Add(wx.StaticLine(p, size=wx.Size(0,0)), 0, wx.ALL | wx.EXPAND, 10)

        # Set sizer to create the layout
        self.SetSizerAndFit(sizer)
        p.Fit()

    def open_files(self, event):
        root_folder_path = self.dir_picker.GetPath()
        search_subdirs = self.search_subdirs_checkbox.GetValue()
        include_mp4 = self.include_mp4_checkbox.GetValue()
        only_mp4 = self.only_mp4_checkbox.GetValue()
        n_files = self.num_files_entry.GetValue()
        self.set_defaults({'path': root_folder_path, 'subdir': search_subdirs, 'number_of_files': n_files})
        allowed_extensions = type_selector(include_mp4,only_mp4)
        keywords = self.wordsearch_textbox.GetValue()
        open_files.main(allowed_extensions, root_folder_path, search_subdirs, n_files, keywords=keywords)

    def get_defaults(self):

        defaults = {'path': 'C://', 'subdir': False, 'number_of_files': 1}

        curr_script_dirpath =  os.path.dirname(os.path.abspath(__file__))
        defaults_json = curr_script_dirpath + "\\defaults.json"
        if os.path.exists(defaults_json):
            with open(defaults_json, 'r') as f:
                defaults = json.load(f)
                return defaults

        return defaults

    def set_defaults(self, new_defaults):

        curr_script_dirpath =  os.path.dirname(os.path.abspath(__file__))
        defaults_json = curr_script_dirpath + "\\defaults.json"
        with open(defaults_json, 'w') as f:
            f.write(json.dumps(new_defaults))


if __name__ == "__main__":
    app = wx.App()
    frame = MainFrame()
    frame.Show(True)
    app.SetTopWindow(frame)
    curr_script_dirpath =  os.path.dirname(os.path.abspath(__file__))
    frame.SetIcon(wx.Icon(curr_script_dirpath + "\\chrome-icon.ico", wx.BITMAP_TYPE_ICO))
    app.MainLoop()
