import json
import os
import webbrowser


def get_chrome_first_time(settings_filepath, intial_page):

    try:
        pth = 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe'
        webbrowser.register('chrome', None, webbrowser.BackgroundBrowser(pth))
        chrome = webbrowser.get('chrome')
        assert chrome.open_new_tab(intial_page) != False
    except AssertionError:
        pth = 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe'
        webbrowser.register('chrome', None, webbrowser.BackgroundBrowser(pth))
        chrome = webbrowser.get('chrome')
        assert chrome.open_new_tab(intial_page) != False

    settings = {'chrome_exe_path': pth}

    with open(settings_filepath, 'w') as f:
        f.write(json.dumps(settings))

    return chrome


def get_chrome_from_settings(settings_filepath):

    with open(settings_filepath, 'r') as f:
        settings = json.load(f)

    webbrowser.register('chrome', None, webbrowser.BackgroundBrowser(settings['chrome_exe_path']))
    chrome = webbrowser.get('chrome')

    return chrome


def get_chrome():

    curr_script_dirpath =  os.path.dirname(os.path.abspath(__file__))
    settings_filepath = f"{curr_script_dirpath}\\settings.json"
    welcome_file = "file:///" + f"{curr_script_dirpath}\\index.html"

    if not os.path.exists(settings_filepath):
        chrome = get_chrome_first_time(settings_filepath, welcome_file)
    else:
        chrome = get_chrome_from_settings(settings_filepath)

    return chrome
