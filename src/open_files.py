from chrome_webbrowser import get_chrome
from random_files import get_all_files, get_random_files


file_protocol = "file:///"


def main(allowed_extensions, path, search_subdirs, n_files, keywords=None):

    all_files = get_all_files(allowed_extensions, path, read_subfolders=search_subdirs, keywords=keywords)
    random_files_list = get_random_files(all_files, n_files)

    chrome = get_chrome()

    for file in random_files_list:
        chrome.open_new_tab(file_protocol+file)


if __name__ == "__main__":
    main(path, search_subdirs, n_files)
