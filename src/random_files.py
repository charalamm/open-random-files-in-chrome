import os
import random
import re

from datetime import datetime

def type_selector(include_mp4,only_mp4):

    if include_mp4 == True:

            allowed_extensions = ("jpeg", "jpg", "png", "webm", "webp", "mp4", "gif")

    elif only_mp4 == True:

            allowed_extensions = ("mp4")


    else:
             allowed_extensions = ("jpeg", "jpg", "png", "webm", "webp", "gif")

    return allowed_extensions


def name_has_keywords(name, keywords=None):

    if not keywords:
        print(name)
        return True

    regex_pattern = keywords.replace(' ', '|')
    return re.search(regex_pattern, name) is not None


def get_all_files(allowed_extensions, path, read_subfolders=False, keywords=None):

    if read_subfolders:

        all_files = [os.path.join(root, name)
                    for root, dirs, files in os.walk(path)
                    for name in files
                    if name.endswith(allowed_extensions) and name_has_keywords(name, keywords)]

    else:

        all_files = [path + f"\\{name}"
                    for name in os.listdir(path)
                    if name.endswith(allowed_extensions) and name_has_keywords(name, keywords)]

    return all_files


def get_random_files(files_list, n_files):

    if n_files > len(files_list):
        print(f'Can not select {n_files} out of {len(files_list)}')
        return []

    # random seed form time:
    time = datetime.now()
    seed = time.year + time.month + time.day + time.hour + time.minute + time.second + time.microsecond
    random.seed(seed)

    random_files = []
    for _ in range(n_files):
        pos = random.randint(0, len(files_list)-1)
        random_files.append(files_list.pop(pos))

    return random_files
